import random
from django.db import models


def get_random_number():
    return random.random()


class ModelApp1(models.Model):
    number = models.FloatField(default=get_random_number)
    create_at = models.DateTimeField(auto_now_add=True)
